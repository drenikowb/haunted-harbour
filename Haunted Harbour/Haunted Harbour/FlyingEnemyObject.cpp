#include "stdafx.h"
#include "FlyingEnemyObject.h"


FlyingEnemyObject::FlyingEnemyObject(PlayerObject *v, int cX, int cY) : EnemyObject("../pics/SeaHag.bmp",200,200)
{
	height = 39;  
	width = 28;  
	hitHeight = 39;  
	hitWidth = 28;
	curCell = 0;  
	endCell = 4;  
	startCell = 0;  
	loopCells = true;  
	xspeed = -5;  
	yspeed = 0;  
	numBullets = 20;  
	radius = 20;
	centerX = cX;
	centerY = cY;
	shootDelay = 30;
	shootTick = 0;
	angle = 0;
	totalHealth = 20;
	currentHealth = 20;
	for (int i = 0; i < numBullets; i++) 
	{ 
		bullets[i] = new GreenBullet(); 
		bullets[i]->maxDistance = 400;
	}
	victor = v;
}

void FlyingEnemyObject::move()
{
	if (!isDead)
	{
		double radians = (double)angle / 180 * 3.1415926535897; //converting degrees to radians
		x = centerX + radius * cos(radians); //trig math to get flying object to move in a circle
		y = centerY + radius * sin(radians); 
		angle += 10;

		shootTick++;
		if (shootTick == shootDelay)
		{
			fire();
			shootTick = 0;
		}
	}
	animate();
}


void FlyingEnemyObject::onHit(BulletObject * b)
{
	x += b->xspeed;
	currentHealth -= b->damage;
	if (currentHealth <= 0)
	{
		startCell = 0;  
		width = 32;  
		height = 28;  
		endCell = 6;  
		curCell = 0;  
		picX = 0;  
		picY = 156;  
		loopCells = false;  
		isDead = true;
	}
}

void FlyingEnemyObject::checkCollisionWithBlock(GraphicsObject * block)
{
}

void FlyingEnemyObject::fire()
{
	double xDistance = abs(x+width/2 - (victor->x+victor->width/2)); //creating 2D direction vector
	double yDistance = abs(y+height - (victor->y+victor->height/2));

	int xSide = 1;
	int ySide = 1;

	//check which side the player is on
	if (victor->x < x)
	{
		xSide = -1;
	}
	if (victor->y < y)
	{
		ySide = -1;
	}

	//determine which side to shoot the bullet
	if (xDistance > yDistance)
	{
		fireBullet(x + width / 2, y + height, 10*xSide, 10 * yDistance / xDistance*ySide);
	}
	else
	{
		fireBullet(x + width / 2, y + height, 10 * xDistance / yDistance*xSide, 10*ySide);
	}
}

FlyingEnemyObject::~FlyingEnemyObject()
{
}
