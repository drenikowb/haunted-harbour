#include "stdafx.h"
#include "LifeBar.h"


LifeBar::LifeBar(PlayerObject *v)
{
	victor = v;
}


void LifeBar::draw(HDC screen)
{
	//make red bar scale based on player health
	int height = 80 * (double)victor->currentHealth / victor->totalHealth;
	//Defines Red and white brushes  
	HBRUSH hbrRed, hbrWhite;  
	//Defines the colors represented by brushes  
	hbrRed = CreateSolidBrush(RGB(255, 0, 0));  
	hbrWhite = CreateSolidBrush(RGB(255, 255, 255));  
	//Tells the screen to use white brush  
	SelectObject(screen, hbrWhite);  
	Rectangle(screen, 10, 10, 20, 90);  
	//Tells the screen to use red brush  
	SelectObject(screen, hbrRed);  
	Rectangle(screen, 10, 90-height, 20, 90);  
	//Deletes the brush from memory  
	DeleteObject(hbrRed);  
	DeleteObject(hbrWhite);
}

LifeBar::~LifeBar()
{
}
