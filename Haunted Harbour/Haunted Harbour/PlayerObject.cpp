#include "stdafx.h"
#include "PlayerObject.h"

// Since PlayerObject is a subclass of GraphicsObject, PlayerObject must tell the GraphicsObject what some of the values, like image, x and y should be. 
PlayerObject::PlayerObject(): WeaponsObject("../pics/ViktorTesla.bmp",100,100)
{
	setStandRight();
	xspeed = 0;
	yspeed = 0;
	numBullets = 10;
	for (int i = 0; i < numBullets; i++)
	{
		bullets[i] = new PurpleBullet();
	}
	hitWidth = 54;
	hitHeight = 42;

	currentHealth = 100;
	totalHealth = 100;
	stoppedLeft = false;
	stoppedRight = true;
}


PlayerObject::~PlayerObject()
{
}

void PlayerObject::move()
{
	prevX = x;
	prevY = y;
	x += xspeed;
	y += yspeed;
	yspeed += GRAVITY;

	if (y > GROUND - height)
	{
		if (state == JUMPLEFT && stoppedLeft == true)
		{
			setStandLeft();
		}
		else if (state == JUMPRIGHT && stoppedRight == true)
		{
			setStandRight();
		}
		else if (state == JUMPLEFT && stoppedLeft == false)
		{
			setMoveLeft();
		}
		else if (state == JUMPRIGHT && stoppedRight == false)
		{
			setMoveRight();
		}
		y = GROUND - height;
		yspeed = 0;
	}
	if ((x - mapPosition < 100 && xspeed < 0) || (x - mapPosition > 350 && xspeed > 0)) //if the player has reached a certain point then we will move the background
	{
		mapPosition += xspeed;
	}
	animate();
}

void PlayerObject::setMoveRight() 
{
	//width of each cell  
	width = 54;  
	//height of each cell  
	height = 42;  
	//the Y position in the sprite strip  
	picY = 0;  
	//the last cell or the number of cells  
	endCell = 9;  
	//whether to loop through cells  
	loopCells = true;  
	//the cell to start at  
	startCell = 1;  
	//the speed of victor (not related to animation)  
	xspeed = 10;  
	picX=60;  
	//victor�s new state  
	state = MOVERIGHT; 
}

void PlayerObject::setMoveLeft() 
{
	width = 54;
	height = 42;
	picY = 42;
	endCell = 9;
	loopCells = true;
	startCell = 1;
	xspeed = -10;
	state = MOVELEFT;
}

void PlayerObject::setStandRight() 
{
	curCell = 0;
	width = 54;
	height = 42;
	picY = 0;
	picX = 0;
	endCell = 0;
	loopCells = false;
	startCell = 0;
	xspeed = 0;
	state = STANDRIGHT;
}

void PlayerObject::setStandLeft() 
{
	curCell = 0;
	width = 54;
	height = 42;
	picY = 42;
	picX = 0;
	endCell = 0;
	loopCells = false;
	startCell = 0;
	xspeed = 0;
	state = STANDLEFT;
}

void PlayerObject::setJumpRight() 
{
	curCell = 0;
	height = 56;
	width = 48;
	endCell = 5;
	loopCells = false;
	picY = 86;
	yspeed = -30;
	state = JUMPRIGHT;
}

void PlayerObject::setJumpLeft() 
{
	curCell = 0;
	height = 56;
	width = 48;
	endCell = 5;
	loopCells = false;
	picY = 146;
	yspeed = -30;
	state = JUMPLEFT;
}

void PlayerObject::setShootRight() 
{
	curCell = 0;
	width = 60;
	height = 38;
	picY = 199;
	endCell = 0;
	loopCells = false;
	startCell = 0;
	xspeed = 0;
	state = SHOOTRIGHT;
}

void PlayerObject::setShootLeft() 
{
	curCell = 0;
	width = 60;
	height = 38;
	picY = 237;
	endCell = 0;
	loopCells = false;
	startCell = 0;
	xspeed = 0;
	state = SHOOTLEFT;
}

bool PlayerObject::isJumping()
{
	return (state == JUMPLEFT || state == JUMPRIGHT);
}

void PlayerObject::checkCollisionWithBlock(GraphicsObject *block)
{
	if (hitTest(block)) //if the playerobject hits a block
	{
		if (prevX + hitWidth <= block->x) //from the left side
		{
			x = block->x - hitWidth;
		}
		else if (prevX >= block->x + block->hitWidth) //from the right side
		{
			x = block->x + block->hitWidth;
		}
		else if (prevY + hitHeight <= block->y) //from the top side
		{
			if (state == JUMPLEFT && stoppedLeft)
			{
				setStandLeft();
			}
			else if (state == JUMPRIGHT && stoppedRight)
			{
				setStandRight();
			}
			else if (state == JUMPLEFT && !stoppedLeft)
			{
				setMoveLeft();
			}
			else if (state == JUMPRIGHT && !stoppedRight)
			{
				setMoveRight();
			}
			y = block->y - hitHeight;
			yspeed = 0;
		}
		else if (prevY >= block->y + block->hitHeight) //from the bottom side
		{
			y = block->y + block->hitHeight + 1;
			yspeed = 0;
		}
	}
}

void PlayerObject::onHit(BulletObject * b)
{
	x += b->xspeed;
	currentHealth -= b->damage;

	if (currentHealth <= 0)
	{
		x = 100;   
		y = 100;   
		yspeed = 0;   
		xspeed = 0;   
		setStandRight();   
		state = STANDRIGHT;   
		currentHealth = totalHealth;   
		mapPosition = 0;
	}
}
