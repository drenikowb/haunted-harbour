#include "stdafx.h"
#include "BulletObject.h"
#include <math.h>

BulletObject::BulletObject(CString filename) : GraphicsObject(filename, 0, 0)
{
	fired = false;
	height = 12;
	width = 12;
	hitHeight = height;
	hitWidth = width;
	distanceTravelled = 0;
	maxDistance = 300;
	xspeed = 0;
	yspeed = 0;
	exploding = false;
	damage = 10;
}


BulletObject::~BulletObject()
{
}

void BulletObject::move()
{
	animate();
	if (exploding && curCell == endCell)
	{
		reset();
	}
	else
	{
		x += xspeed;
		y += yspeed;
		distanceTravelled += abs(xspeed) + abs(yspeed); //absolute value since we want the speed to always be positive
		if (distanceTravelled > maxDistance)
		{
			fired = false;
			distanceTravelled = 0;
		}
	}
}

void BulletObject::setExplode()
{
	//empty because there are unique values to set for each bullet
}

void BulletObject::reset()
{
	//empty because there are unique values to reset for each bullet
}