#include "stdafx.h"
#include "PurpleBullet.h"


PurpleBullet::PurpleBullet(): BulletObject("../pics/PurpleFire.bmp")
{
	height = 12;
	width = 12;
	hitHeight = height;
	hitWidth = width;
}


PurpleBullet::~PurpleBullet()
{
}

void PurpleBullet::setExplode()
{
	picY = 12;
	picX = 0;
	width = 32;
	height = 32;
	x -= 12;
	y -= 12;
	loopCells = false;
	endCell = 2;
	curCell = 0;
	exploding = true;
}

void PurpleBullet::reset()
{
	exploding = false;
	fired = false;
	curCell = 0;
	picY = 0;
	picX = 0;
	width = 12;
	height = 12;
	distanceTravelled = 0;
	endCell = 0;
}