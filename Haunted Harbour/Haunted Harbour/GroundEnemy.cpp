#include "stdafx.h"
#include "GroundEnemy.h"


GroundEnemy::GroundEnemy():EnemyObject("../pics/SkullCrab.bmp", 200, 200)
{
	width = 28;
	height = 35;
	hitWidth = width;
	hitHeight = height;
	xspeed = 5;
	curCell = 0;
	startCell = 0;
	endCell = 10;
	loopCells = true;
	picY = 36;
	isDead = false;
	numBullets = 5;
	totalHealth = 20;
	currentHealth = 20;
	for (int i = 0; i < numBullets; i++)
	{
		bullets[i] = new GreenBullet();
	}
}


GroundEnemy::~GroundEnemy()
{
}

void GroundEnemy::move()
{
	prevX = x;
	prevY = y;
	y += yspeed;
	x += xspeed;
	yspeed += GRAVITY;
	//if the enemy goes past the ground
	if (y > GROUND - height)
	{
		y = GROUND - height;
		yspeed = 0;
	}

	if (!isDead)
	{
		int rNum = rand() % 30;
		if (rNum == 1)
		{
			if (xspeed < 0)
			{
				fireBullet(x + 10, y + 20, -10, 0);
			}
			else if (xspeed > 0)
			{
				fireBullet(x + 10, y + 20, 10, 0);
			}
		}
	}

	animate();
}

void GroundEnemy::onHit(BulletObject * b)
{
	x += b->xspeed;
	currentHealth -= b->damage;
	if (currentHealth <= 0)
	{
		curCell = 0;
		endCell = 0;
		if (xspeed < 0)
		{
			picY = 70;
		}
		else
		{
			picY = 105;
		}
		isDead = true;
		xspeed = 0;
	}
}

void GroundEnemy::checkCollisionWithBlock(GraphicsObject * block)
{
	if (hitTest(block)) //if the playerobject hits a block
	{
		if (prevX + hitWidth <= block->x) //from the left side
		{
			xspeed *= -1;
			x = block->x - hitWidth;
			picY = 0;
		}
		else if (prevX >= block->x + block->hitWidth) //from the right side
		{
			xspeed *= -1;
			x = block->x + block->hitWidth;
			picY = 36;
		}
		else if (prevY + hitHeight <= block->y) //from the top side
		{
			yspeed = 0;
			y = block->y - hitHeight;
		}
		else if (prevY >= block->y + block->hitHeight) //from the bottom side
		{
			yspeed = 0;
			y = block->y + block->hitHeight;
		}
	}
}
