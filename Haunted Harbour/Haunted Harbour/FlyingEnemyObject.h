#pragma once
#include "EnemyObject.h"
#include "GreenBullet.h"
#include "PlayerObject.h"

class FlyingEnemyObject: public EnemyObject
{
public:
	FlyingEnemyObject(PlayerObject *v, int cX, int cY);
	PlayerObject *victor;
	int radius;
	int angle;
	int centerX;
	int centerY;
	int shootTick;
	int shootDelay;
	void move();
	void onHit(BulletObject *b);
	void checkCollisionWithBlock(GraphicsObject *block);
	void fire();
	~FlyingEnemyObject();
};

