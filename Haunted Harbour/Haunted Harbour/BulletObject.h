#pragma once
#include "GraphicsObject.h"
class BulletObject: public GraphicsObject
{
public:
	BulletObject(CString filename);
	int xspeed;
	int yspeed;
	int distanceTravelled;
	int maxDistance;
	bool fired;
	bool exploding;
	int damage;
	void move();
	virtual void setExplode(); //set to virtual to call the methods of the derived classes
	virtual void reset();
	~BulletObject();
};

