#pragma once
#include "WeaponsObject.h"
#include "GreenBullet.h"
#include "EnemyObject.h"
class GroundEnemy: public EnemyObject
{
public:
	GroundEnemy();
	int prevX;
	int prevY;
	void move();
	void onHit(BulletObject *b);
	void checkCollisionWithBlock(GraphicsObject *block);
	~GroundEnemy();
};

