#include "stdafx.h"
#include "GreenBullet.h"


GreenBullet::GreenBullet() : BulletObject("../pics/GreenFire.bmp")
{
	height = 7;
	width = 7;
	hitHeight = height;
	hitWidth = width;
}


GreenBullet::~GreenBullet()
{
}

void GreenBullet::setExplode()
{
	picY = 8;
	picX = 0;
	width = 15;
	height = 15;
	x -= 8;
	y -= 8;
	loopCells = false;
	endCell = 2;
	curCell = 0;
	exploding = true;
}

void GreenBullet::reset()
{
	exploding = false;
	fired = false;
	curCell = 0;
	picY = 0;
	picX = 0;
	width = 7;
	height = 7;
	distanceTravelled = 0;
	endCell = 0;
}