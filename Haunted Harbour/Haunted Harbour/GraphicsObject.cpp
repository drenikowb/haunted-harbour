#include "stdafx.h"
#include "GraphicsObject.h"


GraphicsObject::GraphicsObject(CString filename, int xpos, int ypos, int w, int h)
{
	ID = filename;  // ../ moves back one folder outside of the Debug and Release folders
	width = w;  
	height = h;  
	hitWidth = width;
	hitHeight = height;
	x = xpos;  
	y = ypos; 
	picX = 0;
	picY = 0;
	curCell = 0;
	endCell = 0;
	startCell = 0;
	loopCells = true;
	image.Load(ID); //Loads in the image
}


GraphicsObject::~GraphicsObject()
{
}

void GraphicsObject::draw(HDC offScreenDC)
{
	if (image == NULL) //do not draw if the image is not loaded correctly
	{
		return;
	}

	image.SetTransparentColor(RGB(255, 174, 201)); //sets the pink to transparent for corresponding Red, Green, Blue values

	image.TransparentBlt(offScreenDC, //Destination DC
						x-mapPosition, //x position of the Destination DC
						y, //y position of the Destination DC
						width, //width of the Destination DC
						height, //height of the Destination DC
						picX, //x position of the Source DC
						picY, //y position of the Source DC
						width, //width of the Source DC
						height); //height of the Source DC
}

void GraphicsObject::animate()
{
	//updates the current cell
	curCell += 1;
	//loop back to the beginning
	if (curCell >= endCell)
	{
		if (loopCells == true)
		{
			curCell = startCell;
		}
		else
		{
			curCell = endCell;
		}
		
	}
	//update the drawing position
	picX = width * curCell;
}

//box collision
bool GraphicsObject::hitTest(GraphicsObject *object)
{
	if ((x + hitWidth > object->x) && (x < object->x + object->hitWidth))
	{
		if ((y + hitHeight > object->y) && (y < object->y + object->hitHeight))
		{
			return true;
		}
	}
	return false;
}