// Haunted Harbour.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Haunted Harbour.h"
#include <stdio.h>

//My Includes
#include "GraphicsObject.h"
#include "PlayerObject.h"
#include "BackgroundObject.h"
#include "GroundEnemy.h"
#include "EnemyObject.h"
#include "FlyingEnemyObject.h"
#include "LifeBar.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

//My Forward Declaration of Functions
void setScreen();
void drawScreen(HWND hWnd);
void loadMap();

// My Global Variables
PlayerObject victor;
BackgroundObject background("../pics/Background3.jpg", 0, 0, 5118, 800,0.2);
BackgroundObject ground("../pics/Ground.bmp", 0, GROUND, 774, 128,1);
BackgroundObject startScreen("../pics/TitleScreen.jpg", 0, 0, 700, 550, 0);
bool startGame = false;
GraphicsObject *blockArray[100];
EnemyObject *enemyArray[100];
LifeBar lifebar(&victor);
int numBlocks = 0;
int numEnemies = 0;

int mapPosition = 0;
HDC offScreenDC;

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_HAUNTEDHARBOUR, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_HAUNTEDHARBOUR));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_HAUNTEDHARBOUR));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_HAUNTEDHARBOUR);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, 700, 580, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   //init map
   loadMap();

   setScreen(); // setup screen for double buffering

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
	case WM_KEYDOWN: //when any key is pressed down on the keyboard
		switch (wParam)
		{
		case VK_RIGHT: //if the right key is pressed the player moves right
			if (victor.isJumping())
			{
				victor.picY = 86;
				victor.xspeed = 10;
				victor.state = JUMPRIGHT;
			}
			else
			{
				victor.setMoveRight();
			}
			victor.stoppedRight = false;
			break;
		case VK_LEFT: //if the left key is pressed the player moves left
			if (victor.isJumping())
			{
				victor.picY = 146;
				victor.xspeed = -10;
				victor.state = JUMPLEFT;
			}
			else
			{
				victor.setMoveLeft();
			}
			victor.stoppedRight = true;
			break;
		case VK_UP: //if the up key is pressed the player jumps
			if (victor.state == MOVELEFT || victor.state == STANDLEFT)
			{
				victor.setJumpLeft();
			}
			else if (victor.state == MOVERIGHT || victor.state == STANDRIGHT)
			{
				victor.setJumpRight();
			}
			break;
		case VK_SPACE: //if the space key is pressed the player shoots
			if (victor.state == STANDLEFT)
			{
				victor.setShootLeft();
			}
			else if (victor.state == STANDRIGHT)
			{
				victor.setShootRight();
			}
			if (victor.state == SHOOTLEFT || victor.state == MOVELEFT || victor.state == JUMPLEFT || victor.state == STANDLEFT)
			{
				victor.fireBullet(victor.x, victor.y+10, -20 + victor.xspeed, 0);
			}
			else if (victor.state == SHOOTRIGHT || victor.state == MOVERIGHT || victor.state == JUMPRIGHT || victor.state == STANDRIGHT)
			{
				victor.fireBullet(victor.x + victor.width-15, victor.y + 10, 20 + victor.xspeed, 0);
			}
			break;
		case VK_RETURN:
			if (!startGame)
			{
				startGame = true;
				SetTimer(hWnd, 1, 50, NULL); //setup the timer with an interval of 50 on our window. Every 50 milliseconds the timer will send a WM_TIMER message to WndProc. 
			}
			break;
		}
		break;

	case WM_KEYUP: //when any key is released from being pressed on the keyboard
		switch (wParam)
		{
		case VK_RIGHT:
			if (!victor.isJumping())
			{
				victor.setStandRight();
			}
			victor.stoppedRight = true;
			break;
		case VK_LEFT:
			if (!victor.isJumping())
			{
				victor.setStandLeft();
			}
			victor.stoppedLeft = true;
			break;
		case VK_UP:
			victor.yspeed = 0;
			break;
		case VK_SPACE:
			if (victor.state == SHOOTLEFT)
			{
				victor.setStandLeft();
			}
			else if (victor.state == SHOOTRIGHT)
			{
				victor.setStandRight();
			}
			break;
		}
		break;
	case WM_TIMER: //Timer update loop
		victor.move();
		victor.moveBullets();

		for (int i = 0; i < numBlocks; i++) //for all the blocks we want to check if victor or the bullets collide with the blocks
		{
			victor.checkCollisionWithBlock(blockArray[i]);
			victor.checkBulletCollisionWithBlock(blockArray[i]);
		}

		for (int i = 0; i < numEnemies; i++)
		{
			enemyArray[i]->move();
			enemyArray[i]->moveBullets();
			if (!enemyArray[i]->isDead) //if the enemy is not dead
			{
				victor.checkBulletCollisionWithObject(enemyArray[i]); //check victors bullets colliding with enemies
				enemyArray[i]->checkBulletCollisionWithObject(&victor); //check enemies bullets colliding with victor
			}
			for (int j = 0; j < numBlocks; j++) //for all the blocks
			{
				enemyArray[i]->checkCollisionWithBlock(blockArray[j]); //check if the block and the enemy collide
			}
			
		}
		PostMessage(hWnd, WM_PAINT, 0, 0); //refresh the screen after pressing a key
    case WM_PAINT: //Render loop
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
			if (startGame)
			{
				background.draw(offScreenDC);
				ground.draw(offScreenDC);
				victor.draw(offScreenDC);
				for (int i = 0; i < numBlocks; i++)
				{
					blockArray[i]->draw(offScreenDC);
				}
				
				for (int i = 0; i < numEnemies; i++)
				{
					enemyArray[i]->draw(offScreenDC);
					enemyArray[i]->drawBullets(offScreenDC);
				}
				victor.drawBullets(offScreenDC);
				lifebar.draw(offScreenDC);
			}
			else
			{
				startScreen.draw(offScreenDC);
			}
			drawScreen(hWnd); //swap buffers
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
		for (int i = 0; i < numEnemies; i++)
		{
			delete enemyArray[i];
		}
		for (int i = 0; i < numBlocks; i++)
		{
			delete blockArray[i];
		}
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

void setScreen() //init back buffer
{
	HDC hTempDC;  
	HBITMAP offScreenBMP;  
	hTempDC = GetDC(0);  
	// Initialize buffer image  
	offScreenDC= CreateCompatibleDC(hTempDC);  
	offScreenBMP = CreateCompatibleBitmap(hTempDC, 800, 600);  
	SelectObject(offScreenDC, offScreenBMP);  
	ReleaseDC(0, hTempDC); 
}

void drawScreen(HWND hWnd) //swap buffers
{
	HDC ScreenDC;
	ScreenDC = GetDC(hWnd);
	TransparentBlt(ScreenDC, 0, 0, 700, 550, offScreenDC, 0, 0, 700, 550, RGB(255, 174, 201));
	DeleteDC(ScreenDC);
}

void loadMap()
{
	FILE *file;
	file = fopen("../Levels/Level1.txt","r");

	char section[80];
	int index = 0, x = 0, y = 0;

	while (true)
	{
		fscanf(file, "%s", section); //fscanf is used to read a line from the file. %d is a digit or integer and %s represents a string
		

		if (strcmp(section, "[Victor]") == 0) 
		{
			while (true) 
			{
				fscanf(file, "%d %d %d", &index, &x, &y); //need to be passed as reference so that the variable changes
				if (index == -1)
				{
					break;
				}
				victor.x = x;
				victor.y = y;
			}
		}
		if (strcmp(section, "[Little-Block]") == 0) 
		{
			while (true)
			{
				fscanf(file, "%d %d %d", &index, &x, &y);
				if (index == -1)
				{
					break;
				}
				blockArray[numBlocks] = new GraphicsObject("../pics/SmallCrate.bmp", x, y, 32, 32);
				numBlocks++;
			}
		}      
		if (strcmp(section, "[Big-Block]") == 0) 
		{
			while (true)
			{
				fscanf(file, "%d %d %d", &index, &x, &y);
				if (index == -1)
				{
					break;
				}
				blockArray[numBlocks] = new GraphicsObject("../pics/Crate.bmp", x, y, 64, 64);
				numBlocks++;
			}
		}

		if (strcmp(section, "[Flying-Enemy]") == 0) 
		{
			while (true)
			{
				fscanf(file, "%d %d %d", &index, &x, &y);
				if (index == -1)
				{
					break;
				}
				enemyArray[numEnemies] = new FlyingEnemyObject(&victor, x, y);
				enemyArray[numEnemies]->x = x;
				enemyArray[numEnemies]->y = y;
				numEnemies++;
			}
		}      
		if (strcmp(section, "[Ground-Enemy]") == 0) 
		{
			while (true)
			{
				fscanf(file, "%d %d %d", &index, &x, &y);
				if (index == -1)
				{
					break;
				}
				enemyArray[numEnemies] = new GroundEnemy();
				enemyArray[numEnemies]->x = x;
				enemyArray[numEnemies]->y = y;
				numEnemies++;
			}
		}

		if (strcmp(section, "[End]") == 0)
		{
			break;
		}
	}

	fclose(file);
}