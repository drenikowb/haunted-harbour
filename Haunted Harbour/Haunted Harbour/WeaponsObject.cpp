#include "stdafx.h"
#include "WeaponsObject.h"

WeaponsObject::WeaponsObject(CString filename, int xpos, int ypos) : GraphicsObject(filename, xpos, ypos)
{
	
}


WeaponsObject::~WeaponsObject()
{
}

void WeaponsObject::fireBullet(int x, int y, int xs, int ys)
{
	for (int i = 0; i < numBullets; i++)
	{
		if (bullets[i]->fired == false)
		{
			bullets[i]->x = x;
			bullets[i]->y = y;
			bullets[i]->xspeed = xs;
			bullets[i]->yspeed = ys;
			bullets[i]->fired = true;
			bullets[i]->distanceTravelled = 0;
			break;
		}
	}
}

void WeaponsObject::drawBullets(HDC offScreenDC)
{
	for (int i = 0; i < numBullets; i++)
	{
		if (bullets[i]->fired == true)//accessing properties of a pointer require an arrow instead of a dot
		{
			bullets[i]->draw(offScreenDC);
		}
	}
}

void WeaponsObject::moveBullets()
{
	for (int i = 0; i < numBullets; i++)
	{
		if (bullets[i]->fired == true)
		{
			bullets[i]->move();
		}
	}
}

void WeaponsObject::checkBulletCollisionWithBlock(GraphicsObject *block)
{
	for (int i = 0; i < numBullets; i++)
	{
		if (bullets[i]->fired)
		{
			if (bullets[i]->hitTest(block) && !bullets[i]->exploding) 
			{
				if (abs(bullets[i]->x - block->x) < abs(bullets[i]->x - (block->x + block->width)))
				{
					bullets[i]->x = block->x;
				}
				else if (abs(bullets[i]->x - block->x) > abs(bullets[i]->x - (block->x + block->width)))
				{
					bullets[i]->x = block->x + block->width;
				}
				bullets[i]->setExplode();
			}
		}
	}
}

void WeaponsObject::checkBulletCollisionWithObject(WeaponsObject *object)
{
	for (int i = 0; i < numBullets; i++)
	{
		if (bullets[i]->fired && !bullets[i]->exploding)
		{
			if (bullets[i]->hitTest(object))
			{
				object->onHit(bullets[i]);
				bullets[i]->setExplode();
			}
		}
	}
}