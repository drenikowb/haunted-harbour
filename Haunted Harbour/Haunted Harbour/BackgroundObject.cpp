#include "stdafx.h"
#include "BackgroundObject.h"


BackgroundObject::BackgroundObject(CString filename, int posx, int posy, int w, int h, double ss) : GraphicsObject(filename, posx, posy)
{
	height = h;
	width = w;
	scrollSpeed = ss;
}


BackgroundObject::~BackgroundObject()
{
}

void BackgroundObject::draw(HDC offScreenDC)
{
	if (image == NULL) //do not draw if the image is not loaded correctly
	{
		return;
	}

	int relativeX = (int)((x - mapPosition) * scrollSpeed) % width; //position of the background relative to the screen

	image.SetTransparentColor(RGB(255, 174, 201)); //sets the pink to transparent for corresponding Red, Green, Blue values

	image.TransparentBlt(offScreenDC, //Destination DC
		relativeX, //x position of the Destination DC
		y, //y position of the Destination DC
		width, //width of the Destination DC
		height, //height of the Destination DC
		0, //x position of the Source DC
		0, //y position of the Source DC
		width, //width of the Source DC
		height); //height of the Source DC

	//the second background image that is on the right side
	image.TransparentBlt(offScreenDC, //Destination DC
		relativeX + width, //x position of the Destination DC 
		y, //y position of the Destination DC
		width, //width of the Destination DC
		height, //height of the Destination DC
		0, //x position of the Source DC
		0, //y position of the Source DC
		width, //width of the Source DC
		height); //height of the Source DC

	//the third background image that is on the left side
	image.TransparentBlt(offScreenDC, //Destination DC
		relativeX - width, //x position of the Destination DC
		y, //y position of the Destination DC
		width, //width of the Destination DC
		height, //height of the Destination DC
		0, //x position of the Source DC
		0, //y position of the Source DC
		width, //width of the Source DC
		height); //height of the Source DC
}