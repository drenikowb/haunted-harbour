#pragma once
#include "GraphicsObject.h"
#include "WeaponsObject.h"
#include "PurpleBullet.h"
#define MOVERIGHT 1 
#define MOVELEFT 2 
#define STANDLEFT 3 
#define STANDRIGHT 4 
#define JUMPRIGHT 5 
#define JUMPLEFT 6 
#define SHOOTRIGHT 7 
#define SHOOTLEFT 8 

class PlayerObject: public WeaponsObject //our player is a child of weaponsObject because the player will carry a weapon
{
public:
	PlayerObject();
	int xspeed;
	int yspeed;
	int state;
	bool stoppedRight;
	bool stoppedLeft;
	int prevX;
	int prevY;
	int totalHealth;
	int currentHealth;
	void move();
	void setMoveRight();
	void setMoveLeft();
	void setStandRight();  
	void setStandLeft();  
	void setJumpRight();  
	void setJumpLeft();  
	void setShootRight();  
	void setShootLeft();
	bool isJumping();
	void checkCollisionWithBlock(GraphicsObject *block);
	void onHit(BulletObject *b);
	~PlayerObject();
};

