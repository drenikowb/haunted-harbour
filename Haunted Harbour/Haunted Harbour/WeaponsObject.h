#pragma once
#include "GraphicsObject.h"
#include "BulletObject.h"
class WeaponsObject: public GraphicsObject
{
public:
	WeaponsObject(CString filename, int xpos, int ypos);
	int numBullets;
	BulletObject *bullets[50];
	void fireBullet(int x, int y, int xs, int ys);
	void drawBullets(HDC offScreenDC);
	void moveBullets();
	void checkBulletCollisionWithBlock(GraphicsObject *block);
	void checkBulletCollisionWithObject(WeaponsObject *object);
	virtual void onHit(BulletObject *b) {}; //brace brackets included because this function will not be implemented at this level and only implemented in derived classes. Hence why it's a virtual function 
	~WeaponsObject();
};

