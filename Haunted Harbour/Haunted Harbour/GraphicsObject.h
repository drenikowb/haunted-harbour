#pragma once
class GraphicsObject
{
public:
	GraphicsObject(CString filename, int xpos, int ypos, int w = 55, int h = 42);
	CString ID;
	CImage image;
	int height;
	int width;
	int x;
	int y;
	int picX;
	int picY;
	int curCell;
	int endCell;
	int startCell;
	bool loopCells;
	int hitHeight;
	int hitWidth;
	void draw(HDC offScreenDC);
	void animate();
	bool hitTest(GraphicsObject *object);
	~GraphicsObject();
};

