#pragma once
#include "WeaponsObject.h"
#include "atlimage.h"
class EnemyObject : //abstract class with no .cpp file. abstract classes can be made in the add class wizard by checking the Inline option.
	public WeaponsObject
{
public:
	EnemyObject(CString filename, int xpos, int ypos) : WeaponsObject(filename, xpos, ypos)
	{
		xspeed = 0;
		yspeed = 0;
		isDead = false;
	}
	int xspeed;
	int yspeed;
	bool isDead;
	int totalHealth;
	int currentHealth;
	virtual void move() = 0; //0 means that this function will not be implemented by this class
	virtual void onHit(BulletObject *b) = 0;
	virtual void checkCollisionWithBlock(GraphicsObject *block) = 0;
	~EnemyObject() {}
};

